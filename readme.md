
##Grid 
two-dimensional grid of cells, each of which is in one of two possible states, alive or dead.

Which takes width and height dimension of the grid and position of live cells.

Each position in the grid can be POINT, which stores - x, y position

Surrounding cells of any cell can be obtained by all combination of  addition and deletion on position of cell as direction of move. 


## Types of Cells
1. Live cell - as state 1
2. Dead cell - as state 0

Cell should be Class


### Death Condition for live cell
Any live cell with fewer than two live neighbors dies, as if caused by under-population.
Any live cell with two or three live neighbors lives on to the next generation.
Any live cell with more than three live neighbors dies, as if by over-population.

### Death Condition for Dead cell
Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.


##GameOfLife 
1.Responsible for Initialization of Game
2. Responsible to trigger at regular interval to next step to change (tick)
3. Responsible to listen to start or stop


