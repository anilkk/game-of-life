function Point(x, y) {
    this.x = x;
    this.y = y;
}

function Grid(width, height, liviningCellPositions) {
    this.width = width;
    this.height = height;
    this.cells = new Array(width * height);

    //initialize all cells
    this.each(function(point, value) {
        this.setValueAt(point, 0);
    }.bind(this));

    // initialize living cells
    this.updateLivingCells(liviningCellPositions || []);
}

Grid.prototype.valueAt = function(point) {
    return this.cells[point.y * this.width + point.x];
};
Grid.prototype.setValueAt = function(point, value) {
    this.cells[point.y * this.width + point.x] = value;
};
Grid.prototype.setValueByCellIndex = function(index, value) {
    this.cells[index] = value;
};

Grid.prototype.updateLivingCells = function(liviningCellPositions) {
    var that = this;
    // value looks like '0,1' 0 - x position, 1 - position
    liviningCellPositions.forEach(function(value) {
        var pos = value.split(',');
        that.setValueAt(new Point(parseInt(pos[0]), parseInt(pos[1])), 1);
    });
};

Grid.prototype.each = function(action) {
    var that = this;
    for (var y = 0; y < this.height; y++) {
        for (var x = 0; x < this.width; x++) {
            var point = new Point(x, y);
            action(point, this.valueAt(point));
        }
    }
};

Grid.prototype.isInside = function(point) {
    return point.x >= 0 && point.y >= 0 &&
        point.x < this.width && point.y < this.height;
};

//Every cell interacts with its eight neighbors, 
//which are the cells that are horizontally, vertically, or diagonally adjacent.
var Surroundings = [new Point(0, -1),
    new Point(1, -1),
    new Point(1, 0),
    new Point(1, 1),
    new Point(0, 1),
    new Point(-1, 1),
    new Point(-1, 0),
    new Point(-1, -1)
];


function GameOfLife(x, y, liviningCellPositions) {
    this.grid = new Grid(x, y, liviningCellPositions);
}

//should trigger update of cells status (live / dead) 
GameOfLife.prototype.tick = function() {
    this.updateGridNextState(this.getNextStateOfGrid());
};

GameOfLife.prototype.getNextStateOfGrid = function() {
    var gridNextState = [];
    this.grid.each(function(point, value) {
        var surroundingLivingCells = this.getNumberOfSurroundingLivingCells(point);
        gridNextState.push(this.getCellNextStateByNumberOfSurroundingLivingCells(surroundingLivingCells, value));
    }.bind(this));

    return gridNextState;
};

GameOfLife.prototype.updateGridNextState = function(gridNextState) {
    gridNextState.forEach(function(value, index) {
        this.grid.setValueByCellIndex(index, value);
    }.bind(this));
};


GameOfLife.prototype.getNumberOfSurroundingLivingCells = function(center) {
    return Surroundings.filter(function(surroundingPoint) {
        var neighborPoint = new Point(center.x + surroundingPoint.x, center.y + surroundingPoint.y),
            isLiving = this.grid.valueAt(neighborPoint);

        if (isLiving === 1 && this.grid.isInside(neighborPoint)) {
            return true;
        }
    }.bind(this)).length;
};

//Cell should die or live 
//
//Respect following game rules
// Any live cell with fewer than two live neighbors dies, as if caused by under-population.
// Any live cell with two or three live neighbors lives on to the next generation.
// Any live cell with more than three live neighbors dies, as if by over-population.
// Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
// 
// return boolean value
// 0 - die
// 1 - live
GameOfLife.prototype.getCellNextStateByNumberOfSurroundingLivingCells = function(numberOfSurroundingLivingCells, value) {
	if (value) {
		return (numberOfSurroundingLivingCells === 2 || numberOfSurroundingLivingCells === 3) ? 1 : 0;
	} else {
		return (numberOfSurroundingLivingCells === 3) ? 1 : 0;
	}
};

GameOfLife.prototype.toString = function() {
    var characters = [],
        endOfLine = this.grid.width - 1;

    this.grid.each(function(point, value) {
        characters.push(characterFromCellState(value));
        if (point.x == endOfLine)
            characters.push("\n");
    });
    return characters.join("");
};
GameOfLife.prototype.html = function() {
    var html = '',
        endOfLine = this.grid.width - 1;

    this.grid.each(function(point, value) {
        html += htmlFromCellState(value);
        if (point.x == endOfLine)
            html += '<br>';
    });
    return html;
};

function characterFromCellState(state) {
    if (state === 1) {
        return 'L';
    } else {
        return 'D';
    }
}

function htmlFromCellState(state) {
    if (state === 1) {
        return '<div class="cell live"></div>';
    } else {
        return '<div class="cell dead"></div>';
    }
}
